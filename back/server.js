var express = require('express'),
  app = express(),
  cors = require('cors'),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

var md5 = require('md5');

var assert = require('assert');


//Prueba movimientos
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/clientes?apiKey=jo5LVl25twxRrQLS-yWkTk79ksALx836";
var clienteMlab = requestjson.createClient(urlmovimientosMlab);

//BLOQUE MONGO
var mongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/local";

//BLOQUE Postgres
var pg = require('pg');
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad"



app.use(cors());
app.listen(port);

console.log('API server started on: ' + port);

app.get('/movimientos', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      col.find({}).limit(10).toArray(function(err, docs){
        res.send(docs);
      });
      db.close();
    }
  });
});

app.post('/movimientos',function(){
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('movimientos');

      //insertar 1
      col.insertOne({a:1},function(err,r){
        console.log(r.insertedCount + ' registros insertados');
      });

      //insert multiple documents
      col.insertMany([{a:2},{a:3}],function(err, r){
        console.log(r.insertedCount + ' registros insertados');
      });

      //insertar por body
      col.insertOne(req.body,function(err, r){
        console.log(r.insertedCount + ' registros insertados body');
      });
      db.close();
    }
  });
});
app.post('/movimientosmongo',function(req, res){
  mongoClient.connect(url,function(err,db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      var dnibody = req.body.dni;
      var cuentabody = req.body.idCuent;
      var movimientobody = req.body.movimiento;

      console.log(cuentabody);
      console.log(movimientobody.idMovimiento);
      console.log(movimientobody.importe);



      col.update(
         { "cuentas.idCuenta": parseInt(cuentabody) },
         { $push: { "cuentas.$.movimientos": {"idMovimiento": movimientobody.idMovimiento, "importe": movimientobody.importe, "latitud": movimientobody.latitud, "longitud": movimientobody.longitud} } }
      );

      // col.updateOne(
      //   { dni: dnibody, 'cuentas.$.idCuenta': parseInt(cuentabody) },
      //   {$push: {'cuentas.$.movimientos': {movimientobody}}}
      // );

      res.send("OK");
      db.close();
    }

  });
});

app.get('/cuentas/:idcliente', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      col.find({_id:req.params.idcliente}).toArray(function(err, docs){
        var cuentas = docs.cuentas;
        console.log(req.params.idcliente);
        console.log(docs);
        console.log(cuentas);
        res.send(cuentas);
      });
      db.close();
    }
  });
});

/*app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});*/

app.post('/login', function(req, res){
  //crear cliente Postgre
  var clientPostgre = new pg.Client(urlUsuarios);
  clientPostgre.connect();
  //Hacer consulta
  const query = clientPostgre.query('SELECT COUNT(*) FROM usuarios WHERE login=$1 AND password=$2',[req.body.login,req.body.password], (err,result) =>{
    if(err){
      console.log(err);
      res.send(err);
    }else{
      console.log(result.rows[0]);
      res.send(result.rows[0]);
    }
  });
  //devolver resultado
});

app.post('/loginmongo', function(req, res){

  mongoClient.connect(url, function(err, db) {
    assert.equal(null, err);
    console.log("Connected correctly to server");

    var col = db.collection('clientes');

    col.find({dni:req.body.dni,password:req.body.password}).toArray(function(err, docs) {
      assert.equal(null, err);
      console.log(docs);
      if(docs.length == 1){
        res.send("Login OK");
      }else{
        res.send("Login FAILED");
      }
      db.close();
    });
  });
});

app.get('/clientes', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      col.find({}).toArray(function(err, docs){
        res.send(docs);
      });
      db.close();
    }
  });
});

app.post('/clientes', function(req, res){
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');

      //insertar por body
      col.insertOne(req.body,function(err, r){
        console.log(r.insertedCount + ' registros insertados body');
        res.send(r);
      });
      db.close();
    }
  });
  });

// db.movimientos.find({"cuentas.idCuenta" : "cuenta1"},{"cuentas.idCuenta.$":1, _id:0});

app.get('/clientes/:dni', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      var param = req.params.dni;
      col.find({dni:param}).toArray(function(err, docs){
        console.log(req.params.idcliente);
        res.send(docs);
      });
      db.close();
    }
  });
});

app.get('/clientes/:dni/cuentas', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      var param = req.params.dni;
      col.find({dni:param}).toArray(function(err, docs){
        console.log(docs[0].cuentas);
        res.send(docs[0].cuentas);
      });
      db.close();
    }
  });
});

app.get('/clientes/:dni/cuentas/:ncuenta', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      var param = req.params.dni;
      col.find({dni:param}).toArray(function(err, docs){
        console.log(docs[0].cuentas[req.params.ncuenta]);
        res.send(docs[0].cuentas[req.params.ncuenta]);
      });
      db.close();
    }
  });
});

app.get('/clientes/:dni/cuentas/:ncuenta/movimientos', function(req, res) {
  mongoClient.connect(url, function(err, db){
    if(err){
      console.log(err);
    }else{
      console.log("Connected successfully to server");
      var col = db.collection('clientes');
      var param = req.params.dni;
      col.find({dni:param}).toArray(function(err, docs){
        console.log(docs[0].cuentas[req.params.ncuenta].movimientos);
        res.send(docs[0].cuentas[req.params.ncuenta].movimientos);
      });
      db.close();
    }
  });
});
